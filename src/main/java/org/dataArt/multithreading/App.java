package org.dataArt.multithreading;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Random;

/**
 * Есть два массива целых чисел по 1000 элементов.
 * Заполните массивы произвольными числами, затем отсортируйте массивы в первом потоке сортировкой слиянием,
 * во втором потоке быстрой сортировкой.
 * В третьем потоке найдите среднее арифметическое наибольших 10 элементов из первого потока, и наименьших
 * 100 элементов из второго потока.
 * Так же в результате выполнения программы выведете время работы быстрой сортировкой, и сортировкой слиянием,
 * и в отдельные файлы запишите отсортированные массивы.
 */
public class App {

    private static final String ANSI_RED = "\u001B[31m";
    private static final String ANSI_GREEN = "\u001B[32m";
    private static final String ANSI_BLUE = "\u001B[34m";
    private static final String ANSI_CYAN = "\u001B[36m";
    private static final String ANSI_RESET = "\u001B[0m";

    public static void main(String[] args) {
        int[] array1 = new Random().ints(1_000).toArray();
        int[] array2 = new Random().ints(1_000).toArray();

        System.out.println("\n" + ANSI_BLUE + "======================== 1 array =======================" + "\n");
        System.out.println(Arrays.toString(array1));

        System.out.println("\n" + ANSI_RED + "======================== 2 array ========================" + "\n");
        System.out.println(Arrays.toString(array2));

        System.out.println("\n" + ANSI_GREEN + "===================== Sorted 1 array =================" + "\n");
       MergeSort mergeSort = new MergeSort(array1);
        long start1 = System.currentTimeMillis();
        mergeSort.start();

        try {
            mergeSort.join();
            System.out.println("\n" + "Merge Sort done in: " + (System.currentTimeMillis() - start1) + "ms");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("\n" + ANSI_CYAN + "===================== Sorted 2 array ==================" + "\n");
        QuickSort quickSort = new QuickSort(array2);
        long start2 = System.currentTimeMillis();
        quickSort.start();

        try {
            quickSort.join();
            System.out.println("\n" + "Quick Sort done in: " + (System.currentTimeMillis() - start2) + "ms");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        addArrayToFile(array1, "Array1.txt");

        addArrayToFile(array2, "Array2.txt");

        System.out.println("\n" + ANSI_RESET + "===================== Arithmetic average =====================" + "\n");

        Average average = new Average(array1, array2);
        average.start();
    }

    private static void addArrayToFile(int[] array, String fileName) {
        FileWriter fw;

        try {
            fw = new FileWriter(fileName);

            for (int i1 : array) {
                final String s = Integer.toString(i1);
                fw.write(s);
                fw.write(System.lineSeparator());
            }
            System.out.println("\n" + "Document completed.");
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
