package org.dataArt.multithreading;

import java.math.BigInteger;

/**
 * В третьем потоке найдите среднее арифметическое наибольших 10 элементов из первого потока, и наименьших
 * 100 элементов из второго потока.
 */

public class Average extends Thread {


    private int[] sortedArray1;
    private int[] sortedArray2;
    private int[] result;

    Average(int[] sortedArray1, int[] sortedArray2) {
        this.sortedArray1 = sortedArray1;
        this.sortedArray2 = sortedArray2;

        result = new int[110];

        System.arraycopy(sortedArray1, (1000 - 10), result, 0, 10);
        System.arraycopy(sortedArray2, 0, result, 10, 100);
    }

    @Override
    public void run() {
        BigInteger sum1 = BigInteger.valueOf(0);
        for (int i = sortedArray1.length - 1; i >= (1000 - 10); i--) {
            sum1 = sum1.add(BigInteger.valueOf(sortedArray1[i]));
        }
        System.out.println("The arithmetic average of the largest 10 elements from the first stream = " +
                (sum1.divide(BigInteger.valueOf(10))));

        BigInteger sum2 = BigInteger.valueOf(0);
        for (int i = 0; i < 100; i++) {
            sum2 = sum2.add(BigInteger.valueOf(sortedArray2[i]));
        }
        System.out.println("The arithmetic average of the smallest 100 elements from the second thread = " +
                (sum2.divide(BigInteger.valueOf(100))));

        BigInteger sum3 = BigInteger.valueOf(0);
        for (int i1 : result) {
            sum3 = sum3.add(java.math.BigInteger.valueOf(i1));
        }
        System.out.println("The arithmetic average of the largest 10 elements from the first stream " +
                "and of the smallest 100 elements from the second thread = " + (sum3.divide(BigInteger.valueOf(110))));
    }
}
